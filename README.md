# RDSDS Globus Connect Server 4

git clone https://gitlab.ebi.ac.uk/crsndr/rdsds-globus-connect-server-4.git

Please edit globus-connect-server.conf:

- add your Globus user to be utilized for the configuration on Globus servers (e.g.: User = ebi)
- use CILogon (e.g.: European Molecular Biology Laboratory - EMBL-EBI) or use a different authentication method as MyProxy server.
- select defaul landing directory (e.g.: DefaultDirectory = /gridftp/)
- restrict paths as needed (e.g.: RestrictPaths = R/gridftp,RW/gridftp-rw). Please note that in those examples a directory for RW is created and assigned to a specific user. Only this user will have access to it and will be able to write.


As root user please run:

install_GCS4.sh
make_configuration.sh
