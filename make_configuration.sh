cp globus-connect-server.conf /etc
adduser globus-ebi
mkdir -p /home/globus-ebi
ln -s /home/ /homes

mkdir /gridftp
mkdir /gridftp-rw
chmod 700 /gridftp-rw
chown globus-ebi:globus-ebi /gridftp-rw


echo "Please remember to set a password for globus-ebi (might not be necessary now that CILogon is used)"
