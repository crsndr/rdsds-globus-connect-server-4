curl -LOs https://downloads.globus.org/toolkit/globus-connect-server/globus-connect-server-repo-latest.noarch.rpm
rpm --import https://downloads.globus.org/toolkit/gt6/stable/repo/rpm/RPM-GPG-KEY-Globus
yum install -y globus-connect-server-repo-latest.noarch.rpm
curl -LOs https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum install -y epel-release-latest-7.noarch.rpm
yum install -y yum-plugin-priorities
yum install -y globus-connect-server

rm -vf epel-release-latest-7.noarch.rpm globus-connect-server-repo-latest.noarch.rpm
